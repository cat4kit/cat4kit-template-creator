# CAT4KIT Template Creator by cookiecutter



You can generate a new module (right now in python) with the following commands:

```bash
python -m venv venv
source venv/bin/activate
pip install cookiecutter
cookiecutter https://codebase.helmholtz.cloud/cat4kit/cat4kit-template-creator.git
```
or

```bash
git clone https://codebase.helmholtz.cloud/cat4kit/cat4kit-template-creator.git
pip install -r cat4kit-template-creator/requirements.txt
cookiecutter cat4kit-template-creator/
```