{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}

{% if is_open_source %}
{% if cookiecutter.pypi_username != '' %}
.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_title }}.svg
        :target: https://pypi.python.org/pypi/{{ cookiecutter.project_title }}
{% endif %}
{% if cookiecutter.use_pypi_deployment_with_travis == 'y' %}
.. image:: https://img.shields.io/travis/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_title }}.svg
        :target: https://travis-ci.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_title }}
{% endif %}
.. image:: https://readthedocs.org/projects/{{ cookiecutter.project_title | replace("_", "-") }}/badge/?version=latest
        :target: https://{{ cookiecutter.project_title | replace("_", "-") }}.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status

{%- endif %}

{% if cookiecutter.add_pyup_badge == 'y' %}
.. image:: https://pyup.io/repos/github/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_title }}/shield.svg
     :target: https://pyup.io/repos/github/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_title }}/
     :alt: Updates
{% endif %}


{{ cookiecutter.project_short_description }}

{% if is_open_source %}
* Free software: {{ cookiecutter.open_source_license }}
* Documentation: https://{{ cookiecutter.project_title | replace("_", "-") }}.readthedocs.io.
{% endif %}

Features
--------

* TODO

Copyright
---------
{{ cookiecutter.copyright.replace("(C)", "©") }}

Licensed under the EUPL-1.2-or-later

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the EUPL-1.2 license for more details.

You should have received a copy of the EUPL-1.2 license along with this
program. If not, see https://www.eupl.eu/.