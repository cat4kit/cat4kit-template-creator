from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "{{ cookiecutter.full_name }}"
__copyright__ = "{{ cookiecutter.copyright }}"
__credits__ = [{% for author in cookiecutter.full_name.split(',') %}
    "{{ author.strip()}}",
{% endfor %}]
__license__ = "EUPL-1.2"

__maintainer__ = "{{ cookiecutter.institution }}"
__email__ = "{{ cookiecutter.email }}"

__status__ = "Pre-Alpha"