"""Console script for {{cookiecutter.project_title}}."""
import sys

import click

{%- if cookiecutter.command_line_interface|lower == 'argparse' %}
{%- endif %}
{%- if cookiecutter.command_line_interface|lower == 'click' %}
{%- endif %}

{% if cookiecutter.command_line_interface|lower == 'click' %}
@click.command()
def main(args=None):
    """Console script for {{cookiecutter.project_title}}."""
    click.echo("Replace this message by putting your code into "
               "{{cookiecutter.project_title}}.cli.main")
    click.echo("See click documentation at https://click.palletsprojects.com/")
    return 0
{%- endif %}
{%- if cookiecutter.command_line_interface|lower == 'argparse' %}
import argparse

def main():
    """Console script for {{cookiecutter.project_title}}."""
    parser = argparse.ArgumentParser()
    parser.add_argument('_', nargs='*')
    args = parser.parse_args()

    print("Arguments: " + str(args._))
    print("Replace this message by putting your code into "
          "{{cookiecutter.project_title}}.cli.main")
    return 0
{%- endif %}


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
